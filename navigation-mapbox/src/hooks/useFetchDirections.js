import { useState, useEffect } from "react";
import axios from "axios";
import mapboxgl from "mapbox-gl";
const useFetchDirections = (originMarker, destinationMarker) => {
  const [route, setRoute] = useState([]);

  useEffect(() => {
    if (originMarker.length !== 0 && destinationMarker.length !== 0) {
      console.log("orign", originMarker);
      console.log("des", destinationMarker);
      axios
        .get(
          `https://api.mapbox.com/directions/v5/mapbox/driving/${originMarker[0]},${originMarker[1]};${destinationMarker[0]},${destinationMarker[1]}?steps=true&geometries=geojson&access_token=${mapboxgl.accessToken}`
        )
        .then((resp) => {
          setRoute(resp.data.routes[0].geometry.coordinates);
        });
    }
  }, [originMarker, destinationMarker]);

  return route;
};
export default useFetchDirections;
