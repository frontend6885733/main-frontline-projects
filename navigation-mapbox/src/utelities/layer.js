export const layer = {
  id: "LineString",
  type: "line",
  source: "LineString",
  layout: {
    "line-join": "round",
    "line-cap": "round",
  },
  paint: {
    "line-color": "#BF93E4",
    "line-width": 5,
  },
};
