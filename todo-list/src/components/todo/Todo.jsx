/* eslint-disable react/prop-types */
import "./todo.css";
import deleteIcon from "../../assets/Delete.svg";
import editIcon from "../../assets/Edit.svg";
import Checkbox from "../checkbox/Checkbox";
import { useDispatch } from "react-redux";
import {
  setEditMode,
  setEditedText,
  DecativateEditMode,
} from "../../actions/todoActions";

function Todo({ text, todo, onDelete, onEdit }) {
  const dispatch = useDispatch();

  const handleEdit = (e) => {
    e.preventDefault();
    onEdit(todo.id);
    dispatch(DecativateEditMode(todo.id));
  };
  return (
    <div
      className="todo-card"
      style={todo.read ? { backgroundColor: "var(--background-color)" } : {}}
    >
      <div className="todo-check">
        <Checkbox todo={todo} />
        {todo.editMode ? (
          <form onSubmit={handleEdit} className="edit-form">
            <input
              type="text"
              className="edit-input"
              defaultValue={text}
              onChange={(e) => dispatch(setEditedText(e.target.value))}
            />
          </form>
        ) : (
          <h3
            style={
              todo.read
                ? { textDecoration: "line-through", color: "var(--text-secondary)" }
                : {}
            }
          >
            {text}
          </h3>
        )}
      </div>
      <div className="todo-btn-group">
        <i onClick={() => onDelete(todo.id)}>
          <img src={deleteIcon} alt="delete" />
        </i>
        <i onClick={() => dispatch(setEditMode(todo.id))}>
          <img src={editIcon} alt="edit" />
        </i>
      </div>
    </div>
  );
}

export default Todo;
