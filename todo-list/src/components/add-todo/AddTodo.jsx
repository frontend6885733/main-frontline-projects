/* eslint-disable react/prop-types */
import "./addTodo.css";
import addIcon from "../../assets/Plus.svg";
import { useDispatch, useSelector } from "react-redux";
import {
  selectAddBtn,
  selectIsTodoEmpty,
  selectTodoText,
  selectTodos,
} from "../../selectors";
import {
  addTodo,
  setTodoText,
  onAddTodo,
  emptyTodoCheck,
} from "../../actions/todoActions";
function AddTodo() {
  const dispatch = useDispatch();
  const addBtnClicked = useSelector(selectAddBtn);
  const isTodoEmpty = useSelector(selectIsTodoEmpty);
  const todos = useSelector(selectTodos);
  const todoText = useSelector(selectTodoText);
  const submitTodo = (e) => {
    e.preventDefault();
    if (todoText != "") {
      dispatch(emptyTodoCheck(false));
      dispatch(addTodo([...todos, { id: Date.now(), text: todoText, read: false }]));
    } else dispatch(emptyTodoCheck(true));

    dispatch(setTodoText(""));
  };

  return (
    <div className="add-todo">
      <i onClick={() => dispatch(onAddTodo())}>
        <img className="add-icon" src={addIcon} alt="add todo" />
      </i>
      <form className="add-todo-form" onSubmit={submitTodo}>
        <input
          type="text"
          className="todo-input"
          placeholder="press enter to add todo..."
          style={isTodoEmpty ? { border: "1px solid red" } : {}}
          value={todoText}
          onChange={(e) => dispatch(setTodoText(e.target.value))}
        />
        <div
          className={`todo-revealer`}
          style={addBtnClicked ? { width: "0" } : {}}
        ></div>
      </form>
    </div>
  );
}

export default AddTodo;
