/* eslint-disable react/prop-types */
import { useDispatch, useSelector } from "react-redux";
import Filter from "../filter/Filter";
import "./filters.css";
import { useEffect } from "react";
import {
  applyFilterAll,
  applyFilterCompleted,
  applyFilterPlanned,
} from "../../actions/todoActions";
import { selectTodos } from "../../selectors";
function Filters() {
  const todos = useSelector(selectTodos);
  const dispatch = useDispatch();
  const onFilter = (type) => {
    switch (type) {
      case "Completed":
        dispatch(applyFilterCompleted());
        break;
      case "Planned":
        dispatch(applyFilterPlanned());
        break;
      case "All":
        dispatch(applyFilterAll());
        break;
      default:
        dispatch(applyFilterAll());
        break;
    }
  };

  //TO make sure when we add a new todo our state function gets called
  useEffect(() => {
    onFilter();
  }, [todos]);

  return (
    <section className="filter-section">
      <Filter type="All" number={todos.length} onFilter={onFilter} />
      <Filter
        type="Completed"
        number={todos.filter((todo) => todo.read === true).length}
        onFilter={onFilter}
      />
      <Filter
        type="Planned"
        number={todos.length - todos.filter((todo) => todo.read === true).length}
        onFilter={onFilter}
      />
    </section>
  );
}

export default Filters;
