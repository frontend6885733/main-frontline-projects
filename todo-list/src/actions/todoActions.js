export const setTodoText = (text) => {
  return { type: "SET_TODO_TEXT", payload: text };
};
export const addTodo = (todo) => {
  return { type: "ADD_TODO", payload: todo };
};
export const deleteTodo = (todoId) => {
  return { type: "DELETE_TODO", payload: todoId };
};
export const editTodo = (todoId) => {
  return { type: "EDIT_TODO", payload: todoId };
};
export const setEditedText = (text) => {
  return { type: "SET_EDITED_TEXT", payload: text };
};
export const markAsRead = (todoId) => {
  return { type: "MARK_AS_READ", payload: todoId };
};
export const applyFilterCompleted = () => {
  return { type: "APPLY_FLITER_COMPLETED" };
};
export const applyFilterPlanned = () => {
  return { type: "APPLY_FLITER_PLANNED" };
};
export const applyFilterAll = () => {
  return { type: "APPLY_FILTER_ALL" };
};
export const setEditMode = (todoId) => {
  return { type: "EDIT_MODE", payload: todoId };
};
export const DecativateEditMode = (todoId) => {
  return { type: "DEACTIVATE_EDIT_MODE", payload: todoId };
};
export const onAddTodo = () => {
  return { type: "CLICK_ADD_TODO" };
};
export const emptyTodoCheck = (condition) => {
  return { type: "IS_TODO_INPUT_EMPTY", payload: condition };
};
