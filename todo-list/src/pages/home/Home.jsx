import "./home.css";
import AddTodo from "../../components/add-todo/AddTodo";
import Todo from "../../components/todo/Todo";
import Filters from "../../components/filters/Filters";
import { useSelector, useDispatch } from "react-redux";
import { selectFilteredTodos } from "../../selectors";
import { deleteTodo, editTodo } from "../../actions/todoActions";
function Home() {
  const filteredTodos = useSelector(selectFilteredTodos);
  const dispatch = useDispatch();

  //Delete todo
  const onDelete = (todoId) => {
    dispatch(deleteTodo(todoId));
  };
  //edit todo
  const onEdit = (todoId) => {
    dispatch(editTodo(todoId));
  };
  console.log(filteredTodos);
  return (
    <div className="todo-app">
      <h1>
        My <span>Todo</span> List
      </h1>

      <div className="filter-container">
        <Filters />
      </div>

      <div className="todo-cards">
        <AddTodo />
        {filteredTodos.map((todo) => (
          <Todo
            key={todo.id}
            text={todo.text}
            todo={todo}
            onDelete={onDelete}
            onEdit={onEdit}
          />
        ))}
      </div>
    </div>
  );
}

export default Home;
