const initialState = {
  todos: [
    {
      id: 1,
      text: "Do the laundary",
      read: false,
    },
    {
      id: 2,
      text: "Go out with friends",
      read: false,
    },
    {
      id: 3,
      text: "chill out and watch movies",
      read: false,
    },
  ],
  addBtnClicked: false,
  editMode: false,
  todoText: "",
  editedText: "",
  filteredTodos: [],
};

function todoReducer(state = initialState, action) {
  switch (action.type) {
    case "CLICK_ADD_TODO":
      return { ...state, addBtnClicked: !state.addBtnClicked };
    case "IS_TODO_INPUT_EMPTY":
      return { ...state, isTodoEmpty: action.payload };
    case "ADD_TODO":
      return {
        ...state,
        todos: action.payload,
      };
    case "SET_TODO_TEXT":
      return { ...state, todoText: action.payload };
    case "MARK_AS_READ":
      return {
        ...state,
        todos: state.todos.map((todo) =>
          todo.id === action.payload ? { ...todo, read: !todo.read } : todo
        ),
      };
    case "DELETE_TODO":
      return { ...state, todos: state.todos.filter((todo) => todo.id != action.payload) };
    case "SET_EDITED_TEXT":
      return { ...state, editedText: action.payload };
    case "EDIT_MODE":
      return {
        ...state,
        todos: state.todos.map(
          (todo) =>
            todo.id === action.payload
              ? { ...todo, editMode: true }
              : { ...todo, editMode: false } //this else is to make sure that only one todo is on edit mode at a time
        ),
      };
    case "DEACTIVATE_EDIT_MODE":
      return {
        ...state,
        todos: state.todos.map((todo) =>
          todo.id === action.payload ? { ...todo, editMode: false } : todo
        ),
      };

    case "EDIT_TODO":
      return {
        ...state,
        todos: state.todos.map((todo) =>
          todo.id === action.payload && todo.editMode === true
            ? { ...todo, text: state.editedText }
            : todo
        ),
      };
    case "APPLY_FLITER_COMPLETED":
      return {
        ...state,
        filteredTodos: state.todos.filter((todo) => todo.read === true),
      };
    case "APPLY_FLITER_PLANNED":
      return {
        ...state,
        filteredTodos: state.todos.filter((todo) => todo.read === false),
      };
    case "APPLY_FILTER_ALL":
      return { ...state, filteredTodos: state.todos };

    default:
      return state;
  }
}

export default todoReducer;
