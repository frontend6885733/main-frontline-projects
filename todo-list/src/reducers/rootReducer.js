import { combineReducers } from "redux";
import loginReducer from "./authReducer";
import todoReducer from "./todoReducer";
const rootReducer = combineReducers({
  auth: loginReducer,
  todo: todoReducer,
});

export default rootReducer;
